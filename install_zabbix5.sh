#!/bin/bash
#Deve ser a primeira linha. Ela “diz” para o sistema que se o arquivo é um script executável.
#
# Daqui pra baixo é o mesmo conteudo do DIA 01 com alguns ajustes
#

#Passando o repositório do Zabbix
wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+bionic_all.deb
dpkg -i zabbix-release_5.0-1+bionic_all.deb
apt update


#Instalação do Zabbix Server, Front-end e Agent
apt install zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-agent -y

#importar o schema de Database do Zabbix
zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -pzabbix zabbix

#Acertando as configurações do Database:
#salva o arquivo  /etc/zabbix/zabbix_server.conf original e copia o arquivo modificado
cp  /etc/zabbix/zabbix_server.conf  /etc/zabbix/zabbix_server.conf.bkp
rm  /etc/zabbix/zabbix_server.conf
cp  zabbix_server.conf  /etc/zabbix/zabbix_server.conf

#Acertando as configurações do Front-End do Zabbix:
#salva o arquivo /etc/zabbix/apache.conf original e copia o arquivo modificado
cp /etc/zabbix/apache.conf /etc/zabbix/apache.conf.bkp
cp apache.conf /etc/zabbix/apache.conf


#Reiniciando os serviços e setando para subirem junto com o sistema
systemctl restart zabbix-server zabbix-agent apache2
#Colocando o serviço para iniciar junto com o sistema
systemctl enable zabbix-server zabbix-agent apache2

#envia um aviso sonoro ou visial se estiver em um terminal

echo -en "\a"




#Fim do arquivo
